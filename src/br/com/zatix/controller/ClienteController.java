package br.com.zatix.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import br.com.zatix.dao.ClienteDAO;
import br.com.zatix.model.Cliente;

public class ClienteController {

	public ArrayList<Cliente> listarTodosSensores(ArrayList<Cliente> sensor1, ArrayList<Cliente> max1,
			ArrayList<Cliente> min1, ArrayList<Cliente> media1) throws SQLException {

		System.out.println("Requisição efetuada");
		
		
		ArrayList<Cliente> sensor_5 = ClienteDAO.getInstance().listarTodos5();
		ArrayList<Cliente> sensor_4 = ClienteDAO.getInstance().listarTodos4();
		ArrayList<Cliente> sensor_3 = ClienteDAO.getInstance().listarTodos3();
		ArrayList<Cliente> sensor_2 = ClienteDAO.getInstance().listarTodos2();
		ArrayList<Cliente> sensor_1 = ClienteDAO.getInstance().listarTodos();
		
		
		ArrayList<Cliente> concatena = new ArrayList<Cliente>(sensor_4);
		ArrayList<Cliente> concatena1 = new ArrayList<Cliente>(sensor_3);
		ArrayList<Cliente> concatena2 = new ArrayList<Cliente>(sensor_2);
		ArrayList<Cliente> concatena3 = new ArrayList<Cliente>(sensor_1);

		concatena.addAll(sensor_5);
		concatena1.addAll(concatena);
		concatena2.addAll(concatena1);
		concatena3.addAll(concatena2);
		
		return concatena3;
	}

}
