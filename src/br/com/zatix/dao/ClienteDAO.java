package br.com.zatix.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

import br.com.zatix.factory.ConnectionFactory;
import br.com.zatix.model.Cliente;

public class ClienteDAO extends ConnectionFactory {

	private static ClienteDAO instance;

	public static ClienteDAO getInstance() {
		if (instance == null)
			instance = new ClienteDAO();
		return instance;
	}

	public ArrayList<Cliente> listarTodos() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes = null;

		conexao = criarConexao();

		clientes = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement("SELECT  * FROM " + "( select  ROW_NUMBER() "
					+ "OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
					+ "nrRegistroID,idterminal,datahoraem, DescComplEvt, "
					+ "T1 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T1 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T2 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T2 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),   "
					+ "T3 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T3 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T4 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T4 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),    "
					+ "T5 = (SELECT cast(substring(DescComplEvt, charindex" + "('(T5 = ', DescComplEvt)+5, charindex"
					+ "('�C', DescComplEvt)-7) as text))" + "from tteleevento    where evento=2000   ) exemplo2 "
					+ "WHERE exemplo2.nrRegistroID = 1 and idterminal = 896715");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente = new Cliente();
				Random gerador = new Random();

				cliente.setvariable("Temp1");
				char ch;

				String valida_temp = rs.getString("T1");
				ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					String trata_acento = "SENSOR 1 DESCONFIGURADO";
					cliente.setvalue(trata_acento);
				} else {
					String celsus = rs.getString("T1");
					String valor = (celsus.substring(1, 5));
					Float.parseFloat(valor);
					cliente.setvalue(valor);
				}

				cliente.setserie(gerador.nextInt());

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

				Date date = new Date();
				cliente.settime(sdf.format(date));

				clientes.add(cliente);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes;
	}

	public ArrayList<Cliente> listarTodos2() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement("SELECT  * FROM " + "( select  ROW_NUMBER() "
					+ "OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
					+ "nrRegistroID,idterminal,datahoraem, DescComplEvt, "
					+ "T1 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T1 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T2 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T2 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),   "
					+ "T3 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T3 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T4 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T4 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),    "
					+ "T5 = (SELECT cast(substring(DescComplEvt, charindex" + "('(T5 = ', DescComplEvt)+5, charindex"
					+ "('�C', DescComplEvt)-7) as text))" + "from tteleevento    where evento=2000   ) exemplo2 "
					+ "WHERE exemplo2.nrRegistroID = 1 AND idterminal = 896715");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();
				Random gerador = new Random();

				cliente1.setvariable("Temp2");
				String valida_temp = rs.getString("T2");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 2 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T2");
					cliente1.setvalue(celsus.substring(1, 5));
				}
				cliente1.setserie(gerador.nextInt());

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

				Date date = new Date();
				cliente1.settime(sdf.format(date));

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listarTodos3() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement("SELECT  * FROM " + "( select  ROW_NUMBER() "
					+ "OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
					+ "nrRegistroID,idterminal,datahoraem, DescComplEvt, "
					+ "T1 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T1 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T2 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T2 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),   "
					+ "T3 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T3 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T4 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T4 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),    "
					+ "T5 = (SELECT cast(substring(DescComplEvt, charindex" + "('(T5 = ', DescComplEvt)+5, charindex"
					+ "('�C', DescComplEvt)-7) as text))" + "from tteleevento    where evento=2000   ) exemplo2 "
					+ "WHERE exemplo2.nrRegistroID = 1 AND idterminal = 896715");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();
				Random gerador = new Random();

				cliente1.setvariable("Temp3");

				String valida_temp = rs.getString("T3");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 3 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T3");
					cliente1.setvalue(celsus.substring(1, 5));
				}

				cliente1.setserie(gerador.nextInt());

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

				Date date = new Date();
				cliente1.settime(sdf.format(date));

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listarTodos4() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement("SELECT  * FROM " + "( select  ROW_NUMBER() "
					+ "OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
					+ "nrRegistroID,idterminal,datahoraem, DescComplEvt, "
					+ "T1 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T1 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T2 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T2 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),   "
					+ "T3 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T3 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T4 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T4 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),    "
					+ "T5 = (SELECT cast(substring(DescComplEvt, charindex" + "('(T5 = ', DescComplEvt)+5, charindex"
					+ "('�C', DescComplEvt)-7) as text))" + "from tteleevento    where evento=2000   ) exemplo2 "
					+ "WHERE exemplo2.nrRegistroID = 1 AND idterminal = 896715");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();
				Random gerador = new Random();

				cliente1.setvariable("Temp4");
				String valida_temp = rs.getString("T4");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 4 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T4");
					cliente1.setvalue(celsus.substring(1, 5));
				}
				cliente1.setserie(gerador.nextInt());

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

				Date date = new Date();
				cliente1.settime(sdf.format(date));

				clientes1.add(cliente1);

			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listarTodos5() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement("SELECT  * FROM " + "( select  ROW_NUMBER() "
					+ "OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
					+ "nrRegistroID,idterminal,datahoraem, DescComplEvt, "
					+ "T1 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T1 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T2 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T2 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),   "
					+ "T3 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T3 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),  "
					+ "T4 = (SELECT cast(substring(DescComplEvt, charindex"
					+ "('(T4 = ', DescComplEvt)+5, charindex('�C', DescComplEvt)-7) as text)),    "
					+ "T5 = (SELECT cast(substring(DescComplEvt, charindex" + "('(T5 = ', DescComplEvt)+5, charindex"
					+ "('�C', DescComplEvt)-7) as text))" + "from tteleevento    where evento=2000   ) exemplo2 "
					+ "WHERE exemplo2.nrRegistroID = 1 AND idterminal = 896715");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();
				Random gerador = new Random();

				cliente1.setvariable("Temp5");
				String valida_temp = rs.getString("T5");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 5 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T5");
					cliente1.setvalue(celsus.substring(1, 5));
				}
				cliente1.setserie(gerador.nextInt());

				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
				sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

				Date date = new Date();
				cliente1.settime(sdf.format(date));

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMinSensor1() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MIN (T1) AS T1 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T1 = (SELECT cast(substring(DescComplEvt, charindex('(T1 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T1");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 1 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T1");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor1_min");
					
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMaxSensor1() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MAX (T1) AS T1 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T1 = (SELECT cast(substring(DescComplEvt, charindex('(T1 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T1");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 1 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T1");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor1_max");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMediaSensor1() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT SUM (T1)/720 AS T1 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T1 = (SELECT cast(substring(DescComplEvt, charindex('(T1 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T1");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 1 DESCONFIGURADO");
				} else {

					String celsus = rs.getString("T1");
					cliente1.setvalue(celsus.substring(0, 4));
					cliente1.setunit("C");
					cliente1.setvariable("sensor1_med");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMinSensor2() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MIN (T2) AS T2 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T2 = (SELECT cast(substring(DescComplEvt, charindex('(T2 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T2");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 2 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T2");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor2_min");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMaxSensor2() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MAX (T2) AS T2 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T2 = (SELECT cast(substring(DescComplEvt, charindex('(T2 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T2");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 2 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T2");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor2_max");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMediaSensor2() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT SUM (T2)/720 AS T2 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T2 = (SELECT cast(substring(DescComplEvt, charindex('(T2 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T2");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 2 DESCONFIGURADO");
				} else {

					String celsus = rs.getString("T2");
					cliente1.setvalue(celsus.substring(0, 4));
					cliente1.setunit("C");
					cliente1.setvariable("sensor2_med");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMinSensor3() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MIN (T3) AS T3 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T3 = (SELECT cast(substring(DescComplEvt, charindex('(T3 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T3");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 3 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T3");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor3_min");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMaxSensor3() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MAX (T3) AS T3 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T3 = (SELECT cast(substring(DescComplEvt, charindex('(T3 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T3");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 3 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T3");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor3_max");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMediaSensor3() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT SUM (T3)/720 AS T3 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T3 = (SELECT cast(substring(DescComplEvt, charindex('(T3 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T3");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 3 DESCONFIGURADO");
				} else {

					String celsus = rs.getString("T3");
					cliente1.setvalue(celsus.substring(0, 4));
					cliente1.setunit("C");
					cliente1.setvariable("sensor3_med");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMinSensor4() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MIN (T4) AS T4 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T4 = (SELECT cast(substring(DescComplEvt, charindex('(T4 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T4");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 4 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T4");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor4_min");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMaxSensor4() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MAX (T4) AS T4 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T4 = (SELECT cast(substring(DescComplEvt, charindex('(T4 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T4");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 4 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T4");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor4_max");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMediaSensor4() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT SUM (T4)/720 AS T4 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T4 = (SELECT cast(substring(DescComplEvt, charindex('(T4 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T4");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 4 DESCONFIGURADO");
				} else {

					String celsus = rs.getString("T4");
					cliente1.setvalue(celsus.substring(0, 4));
					cliente1.setunit("C");
					cliente1.setvariable("sensor4_med");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMinSensor5() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MIN (T5) AS T5 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T5 = (SELECT cast(substring(DescComplEvt, charindex('(T5 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T5");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 5 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T5");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor5_min");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMaxSensor5() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT MAX (T5) AS T5 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T5 = (SELECT cast(substring(DescComplEvt, charindex('(T5 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T5");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 5 DESCONFIGURADO");
				} else {
					String celsus = rs.getString("T5");
					cliente1.setvalue(celsus);
					cliente1.setunit("C");
					cliente1.setvariable("sensor5_max");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

	public ArrayList<Cliente> listaMediaSensor5() throws SQLException {

		Connection conexao = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		ArrayList<Cliente> clientes1 = null;

		conexao = criarConexao();

		clientes1 = new ArrayList<Cliente>();

		try {
			pstmt = conexao.prepareStatement(
					"SELECT SUM (T5)/720 AS T5 FROM ( select  ROW_NUMBER() OVER(PARTITION BY idterminal ORDER BY datahoraem DESC) "
							+ "nrRegistroID,idterminal,datahoraem, DescComplEvt,"
							+ " T5 = (SELECT cast(substring(DescComplEvt, charindex('(T5 = ', DescComplEvt)+6, charindex('�C', DescComplEvt)-08) as FLOAT))"
							+ "from tteleevento    where evento=2000) "
							+ "exemplo2 WHERE  idterminal = 896715 AND nrRegistroID BETWEEN 1 AND 720");

			rs = pstmt.executeQuery();

			while (rs.next()) {

				Cliente cliente1 = new Cliente();

				String valida_temp = rs.getString("T5");
				char ch = valida_temp.charAt(valida_temp.length() - 1);

				if (ch == '.') {
					cliente1.setvalue("SENSOR 5 DESCONFIGURADO");
				} else {

					String celsus = rs.getString("T5");
					cliente1.setvalue(celsus.substring(0, 4));
					cliente1.setunit("C");
					cliente1.setvariable("sensor5_med");
				}

				clientes1.add(cliente1);
			}

		} catch (Exception e) {
			System.out.println("Erro ao listar todos as informacoes: " + e);
			e.printStackTrace();
		} finally {

			conexao.close();
			pstmt.close();
			rs.close();
		}
		return clientes1;
	}

}
