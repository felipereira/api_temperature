package br.com.zatix.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	// Caminho do banco de dados.
	String connectionUrl = "jdbc:sqlserver://10.40.0.115:1433;" + "databaseName=dbOmnilink";

	public Connection criarConexao() {

		Connection conn = null;

		try {

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
			conn = DriverManager.getConnection(connectionUrl, "sa", "");

		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError:" + ex.getErrorCode());

		} catch (Exception e) {
			System.out.println("N�o foi poss�vel conectar");
		}
		return conn;
	}
}
