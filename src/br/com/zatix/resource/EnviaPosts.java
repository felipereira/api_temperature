package br.com.zatix.resource;

public class EnviaPosts {

	public static void main(String[] args) throws Exception {

		Post_Sensor1 post1 = new Post_Sensor1();
		Post_Sensor2 post2 = new Post_Sensor2();
		Post_Sensor3 post3 = new Post_Sensor3();
		Post_Sensor4 post4 = new Post_Sensor4();
		Post_Sensor5 post5 = new Post_Sensor5();
		
		Min_Sensor1 minSensor1 = new Min_Sensor1();
		Max_Sensor1 maxSensor1 = new Max_Sensor1();
		Media_Sensor1 medSensor1 = new Media_Sensor1();
		
		Min_Sensor2 minSensor2 = new Min_Sensor2();
		Max_Sensor2 maxSensor2 = new Max_Sensor2();
		Media_Sensor2 medSensor2 = new Media_Sensor2();
		
		Min_Sensor3 minSensor3 = new Min_Sensor3();
		Max_Sensor3 maxSensor3 = new Max_Sensor3();
		Media_Sensor3 medSensor3 = new Media_Sensor3();
		
		Min_Sensor4 minSensor4 = new Min_Sensor4();
		Max_Sensor4 maxSensor4 = new Max_Sensor4();
		Media_Sensor4 medSensor4 = new Media_Sensor4();
		
		Min_Sensor5 minSensor5 = new Min_Sensor5();
		Max_Sensor5 maxSensor5 = new Max_Sensor5();
		Media_Sensor5 medSensor5 = new Media_Sensor5();
		
		int control = 0;
		do {
			
			post1.testaEnviaPost();
			post2.testaEnviaPost();
			post3.testaEnviaPost();
			post4.testaEnviaPost();
			post5.testaEnviaPost();
			
			minSensor1.calcMinSensor1();
			maxSensor1.calcMaxSensor1();
			medSensor1.calcMedSensor1();
			
			minSensor2.calcMinSensor2();
			maxSensor2.calcMaxSensor2();
			medSensor2.calcMedSensor2();
			
			minSensor3.calcMinSensor3();
			maxSensor3.calcMaxSensor3();
			medSensor3.calcMedSensor3();
			
			minSensor4.calcMinSensor4();
			maxSensor4.calcMaxSensor4();
			medSensor4.calcMedSensor4();
			
			minSensor5.calcMinSensor5();
			maxSensor5.calcMaxSensor5();
			medSensor5.calcMedSensor5();

			control++;

			System.out.println("Disparos feitos:" + control);
			Thread.sleep(120000);

		} while (control != 0);

	}
}
