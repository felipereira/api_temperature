package br.com.zatix.resource;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.zatix.controller.ClienteController;
import br.com.zatix.model.Cliente;

@Path("/temperatura")
public class ClienteResource {

	@GET
	@Path("/listarSensores")
	@Produces("application/json")
	public ArrayList<Cliente> listarTodos() throws SQLException {	
		
		return  new ClienteController().listarTodosSensores(null, null, null, null);
	}

	@Path("/jsonServices")
	public class JerseyRestService {

		@POST
		@Path("/send")
		@Consumes(MediaType.APPLICATION_JSON)
		public Response consumeJSON(Cliente cliente) {

			String output = cliente.toString();

			return Response.status(200).entity(output).build();
		}

	}

}
