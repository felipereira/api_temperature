package br.com.zatix.resource;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;

import com.google.gson.Gson;

import br.com.zatix.dao.ClienteDAO;

public class CriaJson {

	String CriandoJson1() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listarTodos());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String CriandoJson2() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listarTodos2());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String CriandoJson3() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listarTodos3());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String CriandoJson4() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listarTodos4());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String CriandoJson5() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listarTodos5());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMinSensor1() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMinSensor1());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMaxSensor1() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMaxSensor1());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMediaSensor1() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMediaSensor1());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMinSensor2() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMinSensor2());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMaxSensor2() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMaxSensor2());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMediaSensor2() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMediaSensor2());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMinSensor3() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMinSensor3());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMaxSensor3() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMaxSensor3());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMediaSensor3() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMediaSensor3());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMinSensor4() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMinSensor4());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMaxSensor4() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMaxSensor4());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMediaSensor4() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMediaSensor4());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMinSensor5() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMinSensor5());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMaxSensor5() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMaxSensor5());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}

	String calcMediaSensor5() throws SQLException {

		ClienteDAO obj1 = new ClienteDAO();
		Gson gson = new Gson();

		String json1 = gson.toJson(obj1.listaMediaSensor5());

		try {
			FileWriter writer = new FileWriter("Documents:\file.json");
			writer.write(json1);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return json1;
	}
}
