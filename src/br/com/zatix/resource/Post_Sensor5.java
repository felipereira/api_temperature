package br.com.zatix.resource;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class Post_Sensor5 extends Thread {
	private final String USER_AGENT = "Mozilla/5.0";

	/*
	 * public synchronized void run() { MetodoPost1 teste = new MetodoPost1();
	 * try { teste.PostTemp1(); } catch (Exception e) { e.printStackTrace(); } }
	 */
	void testaEnviaPost() throws Exception {

		Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress("200.185.141.207", 8080));

		String url = "https://api.tago.io/data";

		URL obj2 = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj2.openConnection(proxy);
		System.setProperty("java.net.preferIPv4Stack", "true");

		// headers para simular uma solicitação de navegador
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Device-Token", "0f48e032-5207-4e95-91eb-a2a8634a0781");

		CriaJson criajson1 = new CriaJson();
		String enviajson1 = criajson1.CriandoJson5();

		// Envia o Post
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(enviajson1);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("Enviado o metodo 'POST1' do rastreador 896715 para a URL : " + url);
		System.out.println("Post5 : " + enviajson1);
		System.out.println("Resposta : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String output;
		StringBuffer response = new StringBuffer();

		while ((output = in.readLine()) != null) {
			response.append(output);
		}
		in.close();
		con.disconnect();
		// printa a resposta do servidor
		System.out.println(response.toString());
	}

}
