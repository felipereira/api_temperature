package br.com.zatix.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Cliente{

	private int serie;
	private String variable;
	private String value;
	private String unit;
	private String T1;
	private String T2;
	private String T3;
	private String T4;
	private String T5;
	private String time;
	private String tempo;
	
	public int getserie() {
		return this.serie;
	}

	public void setserie(int i) {
		this.serie = i;
	}

	public String getT1() {
		return this.T1;
	}

	public void setT1(String novo) {
		this.T1 = novo;
	}

	public String getT2() {
		return this.T2;
	}

	public void setT2(String novo) {
		this.T2 = novo;
	}

	public String getT3() {
		return this.T3;
	}

	public void setT3(String novo) {
		this.T3 = novo;
	}

	public String getT4() {
		return this.T4;
	}

	public void setT4(String novo) {
		this.T4 = novo;
	}

	public String getT5() {
		return this.T5;
	}

	public void setT5(String novo) {
		this.T5 = novo;
	}

	public String getvariable() {
		return this.variable;

	}

	public void setvariable(String novo) {
		this.variable = novo;
	}

	public String getvalue() {
		return this.value;

	}

	public void setvalue(String num) {
		this.value = num;
	}

	public String getunit() {
		return this.unit;

	}

	public void setunit(String novo) {
		this.unit = novo;

	}

	public String gettime() {
		return this.time;

	}

	public void settime(String string) {
		this.time = string;

	}
	public String gettempo() {
		return this.tempo;

	}

	public void settempo(String string) {
		this.tempo= string;

	}

}
